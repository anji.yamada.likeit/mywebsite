
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>商品マスタ</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
    crossorigin="anonymous">
    <link rel="stylesheet" href="itemMaster.css">
  </head>
  <body>
    <div class="container">
      <div class="header">
        <h1>Fashion EC Master</h1><br>
          <a href="ItemRegistMaster"><button class="btn btn-primary" type="button">商品新規登録</button></a>
        <h2 style="text-align: center;">商品マスタ一覧</h2>
        <br><p style="text-align: center;">登録件数 ${theNumberOfItems }件</p>
      </div>

      <table class="table table-hover">
        <thead>
          <tr>
          	<th class="center"></th>
            <th class="center">商品名</th>
            <th class="center">商品ID</th>
            <th class="center">値段</th>
            <th class="center">在庫数</th>
            <th class="center"></th>
            <th class="center"></th>
            <th class="center"></th>
          </tr>
        </thead>
        <tbody>
        <c:forEach items="${itemList }" var="item">
          <tr>
          	<td class="center"><img alt="" src="/MyWebSite/image/${item.image }" style="height: 60px; width: 50px;"></td>
            <td class="center">${item.name }</td>
            <td class="center">${item.id }</td>
            <td class="center">${item.formatPrice }円</td>
            <td class="center">${item.stock }個</td>
            <td class="center"><a href="ItemDetailMaster?id=${item.id }">詳細</a></td>
            <td class="center"><a href="ItemUpdateMaster?id=${item.id }">更新</a></td>
            <td class="center"><a href="ItemDeleteMaster?id=${item.id }">削除</a></td>
          </tr>
          </c:forEach>
        </tbody>
      </table>
      <a href="Index">商品一覧に戻る</a>
    </div>
  </body>
</html>