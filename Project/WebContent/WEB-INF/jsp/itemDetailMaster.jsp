<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>商品詳細マスタ</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">

      <div class="header">
        <h1>Fashion EC Master</h1><br>
        <h2 style="text-align: center;">商品詳細</h2>
      </div><br>

      <div style="display: flex;">
	      <!-- 画像側（画面左） -->
	      <div class="image" style="margin-right: 20px;">
	      	<img alt="該当する画像がありません" src="/MyWebSite/image/${itemInfo.image }" style="height: 500px; width: 400px">
	      </div>

	      <!-- 詳細側（画面右） -->
	      <div class="itemInfo">
	      	<p>${itemInfo.name }</p>
	      	<p>${itemInfo.formatPrice }円</p>
	      	<br><p>${itemInfo.detail }</p>
	      </div>
      </div>

      <br><a href="ItemMaster">戻る</a>
    </div>
  </body>
</html>