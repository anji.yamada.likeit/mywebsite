<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>購入履歴詳細</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" 
      integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" 
      crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
      <h1>Fashion EC</h1><br>
      <form action="UserDelete?id=${userInfo.id }" method="POST">
        <button type="submit" class="btn btn-primary btn-lg" value="delete" name="userDelete">退会</button>
        <button type="submit" class="btn btn-secondary btn-lg">キャンセル</button>
      </form>
      <br><a href="userDetail.jsp">戻る</a>
    </div>
  </body>
</html>