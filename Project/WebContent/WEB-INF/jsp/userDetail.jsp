<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>会員情報</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
  </head>
  <body>
  <div class="container">
    <h1>Fashion EC</h1><br>

    <!-- 会員情報 -->
    <h2>会員情報</h2>
    <br><h4 style="color: red;">${errorMessage }</h4>
    <br>

    <br><table class="table table-hover">
    	<thead>
    		<tr>
    			<th class="center">ログインID</th>
    			<th class="center">Eメールアドレス</th>
    			<th class="center">登録日</th>
    			<th class="center"></th>
    		</tr>
    	</thead>
    	<tbody>
    		<tr>
    			<td class="center">${userDetail.loginId }</td>
    			<td class="center">${userDetail.emailAddress }</td>
    			<td class="center">${userDetail.createDate }</td>
    			<td class="center"><a href="UserUpdate"><button type="button" class="btn btn-outline-primary">会員情報更新</button></a></td>
    		</tr>
    	</tbody>
    </table><br>

    <!-- 購入履歴 -->
   <br><h2>購入履歴</h2><br>
     <table class="table table-hover">
        <thead>
          <tr>
            <th class="center">購入日</th>
            <th class="center">合計金額</th>
            <th class="center"></th>
          </tr>
        </thead>
        <tbody>
        <c:forEach var="buyInfo" items="${buyHistoryList }">
            <tr>
             <td class="center">${buyInfo.formatDate }</td>
             <td class="center">${buyInfo.formatTotalPrice }円</td>
             <td class="center"><a href="BuyHistoryDetail?buyId=${buyInfo.id }">詳細</a></td>
          </tr>
         </c:forEach>
        </tbody>
      </table><br>

    <br><a href="Index">戻る</a>
  </div>
  </body>
</html>