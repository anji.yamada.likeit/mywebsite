<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>検索結果</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
    crossorigin="anonymous">
  </head>
  <body>
  <div class="container">
    <h1>Fashion EC</h1>
    <div style="text-align: center;">
      <br><h2>検索結果</h2>
      <p>${itemTotalNum }件</p>
      <p>${errorMessage }</p><br>

     <div class="row">
		<c:forEach var="item" items="${searchItemList }">
	      <div class="text-center" style="float: left; width: 25%; ">
	        <br><a href="ItemDetail?id=${item.id }">
	          <img src="${item.image }" class="rounded" alt="該当する画像がありません" style="width: 200px; height: 300px;">
	        </a>
	        <p>${item.name }</p><p>${item.formatPrice }円</p>
	      </div>
		</c:forEach>
	   </div>


    </div>
    <a href="Index">戻る</a>
   </div>
  </body>
</html>