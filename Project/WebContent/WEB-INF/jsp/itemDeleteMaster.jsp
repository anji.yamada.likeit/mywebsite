<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>商品削除マスタ</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
      <div class="header">
        <h1>Fashion EC Master</h1><br>
        <div style="text-align: center;">
          <h2 >商品削除</h2><br>
          <p>${deleteActionMessage }</p>
          <form action="ItemDeleteMaster" method="post">
          	<input type="hidden" value="${itemInfo.id }" name="id">
            <button type="submit" class="btn btn-primary btn-lg" value="delete" name="deleteAction">削除</button>
            <button type="submit" class="btn btn-secondary btn-lg">キャンセル</button>
          </form>
        </div><br>
        <table class="table table-hover">
          <thead>
            <tr>
              <th class="center"></th>
              <th class="center">商品名</th>
              <th class="center">在庫数</th>
              <th class="center">値段</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="center"><img src="/MyWebSite/image/${itemInfo.image }"
                class="rounded" alt="..." width="25%"></td>
              <td class="center">${itemInfo.name }</td>
              <td class="center">${itemInfo.stock }個</td>
              <td class="center">${itemInfo.formatPrice }円</td>
            </tr>
          </tbody>
        </table>
        <a href="ItemMaster">戻る</a>
    </div>
  </body>
</html>