<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>カート</title>
    <link href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
      <h1>Fashion EC</h1>
      <br><p>${cartActionMessage }</p>
      <p>${sameItem }</p>
      <br><h2 style="text-align: center;">カート</h2>
      <table class="table table-hover">
        <thead>
          <tr>
            <th class="center"></th>
            <th class="center">商品名</th>
            <th class="center">値段</th>
            <th class="center">削除</th>
          </tr>
        </thead>
        <tbody>
        	<c:forEach var="item" items="${cart }">
	          <tr>
	            <td class="center"><a href="ItemDetail?id=${item.id }"><img src="${item.image }"
	              class="rounded" alt="..." width="30%"></a></td>
	            <td class="center">${item.name }</td>
	            <td class="center">${item.formatPrice }円</td>
	            <td class="center"><a href="ItemDelete?id=${item.id }"><i class="far fa-trash-alt fa-2x"></i></a></td>
	          </tr>
        	</c:forEach>
        </tbody>
      </table>
      <a href="Index">
        <button type="button" class="btn btn-secondary" formaction="index.html">TOPに戻る</button>
       </a>
	    <form action="BuyConfirm" method="post">
		  <div class="d-grid gap-2 col-6 mx-auto">
		    <button class="btn btn-primary" type="submit" value="">レジに進む</button>
		  </div>
		</form>
  </body>
</html>
