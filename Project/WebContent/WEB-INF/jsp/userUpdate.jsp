<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>会員情報更新</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
      <h1>Fashion EC</h1><br>
      <h2>会員情報更新</h2>
      <p style="color: red;">${errorMessage }</p>
      <br><p>ログインId : </p>
      <p>${userInfo.loginId }</p>
      <form action="UserUpdate?id=${userInfo.id }" method="post">
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Eメールアドレス :</label>
          <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="emailAddress">
        </div>
        <div class="mb-3">
          <label for="exampleInputPassword1" class="form-label">パスワード :</label>
          <input type="password" class="form-control" id="exampleInputPassword1" name="password">
        </div>
        <div class="mb-3">
          <label for="exampleInputPassword1" class="form-label">パスワード確認 :</label>
          <input type="password" class="form-control" id="exampleInputPassword1" name="passwordConfirmation">
        </div>
        <br><p>登録日 : </p>
        <p>${createDate.createDate }</p>

        <br><button type="submit" class="btn btn-primary">更新</button>
      </form>

      <br><a href="UserDetail">戻る</a>
      </div>
  </body>
</html>
