<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>商品詳細</title>
    <link href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
      <div class="header">
        <h1>Fashion EC</h1><br>
        <p>${message }</p><br>
        <form action="Cart" method="post">
       		<button type="submit" class="btn btn-primary btn-sm" value="${itemDetail.id }" name="Id"><i class="fas fa-shopping-cart"></i>カートに入れる</button>
        </form>
        <h2 style="text-align: center;">商品詳細</h2>
      </div><br>

      <div class="item" style="display: flex;">
	      <!-- 画像側（画面左） -->
	      <div class="image" style="margin-right: 20px;">
	      	<img alt="該当する画像がありません" src="${itemDetail.image }" style="height: 550px; width: 450px">
	      </div>

	      <!-- 詳細側（画面右） -->
	      <div class="itemInfo">
	      	<p>${itemDetail.name }</p>
	      	<p>${itemDetail.formatPrice }円</p>
	      	<br><p>${itemDetail.detail }</p>
	      </div>
      </div>
    </div>
  </body>
</html>