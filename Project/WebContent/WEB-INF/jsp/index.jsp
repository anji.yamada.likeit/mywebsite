<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>TOP（商品一覧）</title>
    <link href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
    crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
      <h1>Fashion EC</h1>
      <c:if test="${userInfo != null }">
      <br><p>${userInfo.loginId }さん</p><br>
      </c:if>
      <br><a href="Login"><button type="button" class="btn btn-outline-primary">ログイン</button></a>
      <a href="Logout"><button type="button" class="btn btn-outline-primary">ログアウト</button></a>
      <a href="Cart"><button type="button" class="btn btn-outline-primary">カート</button></a>
      <a href="SignUp"><button type="button" class="btn btn-outline-primary">会員登録</button></a>
      <a href="UserDetail"><button type="submit" class="btn btn-outline-primary">会員情報</button></a>

      <c:if test="${userInfo.loginId == 'admin' }">
      	<a href="ItemMaster"><button type="button" class="btn btn-success">商品マスタ</button></a><br>
      </c:if>
       <br><form action="SearchResult" method="post">
         <input type="text" placeholder="商品検索" name="searchKeyword">
         <i class="fas fa-yen-sign"></i><input type="text" placeholder="値段を入力" name="price1">~
         <i class="fas fa-yen-sign"></i><input type="text" placeholder="値段を入力" name="price2">
         <button type="submit" class="btn btn-secondary"><i class="fas fa-search"></i></button>
       </form>

       <div class="row">
		<c:forEach var="item" items="${itemList }"  varStatus="status">
	      <div class="text-center" style="float: left; width: 25%; ">
	        <br><a href="ItemDetail?id=${item.id }">
	          <img src="${item.image }" class="rounded" alt="該当する画像がありません" style="width: 250px; height: 300px;">
	        </a>
	        <p>${item.name }</p><p>${item.formatPrice }円</p>
	      </div>
		</c:forEach>
	   </div>

    </div>
  </body>
</html>