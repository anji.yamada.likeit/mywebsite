<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>購入確定画面</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
    crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
      <h1>Fashion EC</h1>
      <br><h2 style="text-align: center;">購入</h2><br>
      <table class="table table-hover">
        <thead>
          <tr>
            <th class="center">商品名</th>
            <th class="center">単価</th>
            <th class="center">小計</th>
          </tr>
        </thead>
        <tbody>
         <c:forEach var="item" items="${cart }">
	          <tr>
	            <td class="center">${item.name }</td>
	            <td class="center">${item.formatPrice }円</td>
	            <td class="center">${item.formatPrice }円</td>
	          </tr>
          </c:forEach>
          <tr>
            <td class="center">${deliveryMethod.name }</td>
            <td class="center"></td>
            <td class="center">${deliveryMethod.price }円</td>
          </tr>
           <tr>
            <td class="center">消費税</td>
            <td class="center"></td>
            <td class="center">10%</td>
          </tr>
          <tr>
            <td class="center"></td>
            <td class="center">合計</td>
            <td class="center">${formatTotalPrice }円</td>
          </tr>
        </tbody>
      </table><br>
      <a href="BuyResult?id=${userInfo.id }&deliveryMethodId=${deliveryMethod.id}">
	      <div class="d-grid gap-2 col-6 mx-auto">
	        <button class="btn btn-primary" type="button">購入</button>
	      </div>
      </a><br>
      <a href="Cart">
        <button type="button" class="btn btn-secondary" formaction="Cart">カートに戻る</button>
      </a>
    </div>
  </body>
</html>