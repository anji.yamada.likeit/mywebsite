package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.DeliveryMethodBeans;

public class DeliveryMethodDao {
	public DeliveryMethodBeans findDeliveryMethod() {
		Connection con = null;
        try {
        	con = DBManager.getConnection();
        	String sql = "select * from delivery_method";

        	Statement stmt = con.createStatement();
        	ResultSet rs = stmt.executeQuery(sql);

        	DeliveryMethodBeans deliveryMethodInfo = new DeliveryMethodBeans();

        	if (rs.next()) {
        		deliveryMethodInfo.setId(rs.getInt("id"));
        		deliveryMethodInfo.setName(rs.getString("name"));
        		deliveryMethodInfo.setPrice(rs.getInt("price"));
        	}

        	return deliveryMethodInfo;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }

	}
}
