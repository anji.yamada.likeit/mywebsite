package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.BuyBeans;
import model.BuyDetailBeans;
import model.ItemBeans;

public class BuyDetailDao {
	public void insertBuyDetail(BuyDetailBeans buyDetail) {

		Connection con = null;
        try {
        	con = DBManager.getConnection();
        	String sql = "insert into buy_detail (buy_id, item_id) values (?, ?)";
        	PreparedStatement pstmt = con.prepareStatement(sql);
        	pstmt.setInt(1, buyDetail.getBuyId());
        	pstmt.setInt(2, buyDetail.getItemId());
        	pstmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	public BuyBeans getBuyDataByBuyId(String buyId) {
		Connection con = null;
		BuyBeans buyData = new BuyBeans();
        try {
        	con = DBManager.getConnection();
        	String sql = "select buy_date, total_price, name, price from buy"
        			+ " join delivery_method on buy.delivery_method_id = delivery_method.id where buy.id = ?";
        	PreparedStatement ps = con.prepareStatement(sql);
        	ps.setString(1, buyId);
        	ResultSet rs = ps.executeQuery();
        	if (rs.next()) {
        		buyData.setBuyDate(rs.getTimestamp("buy_date"));
        		buyData.setTotalPrice(rs.getInt("total_price"));
        		buyData.setDeliveryMethodName(rs.getString("name"));
        		buyData.setDeliveryMethodPrice(rs.getInt("price"));
        	}
        	return buyData;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
	}

	//buyidによる購入詳細情報取得
	public ArrayList<ItemBeans> getBuyDetailByBuyId(String buyId) {
		Connection con = null;
		ArrayList<ItemBeans> itemList = new ArrayList<ItemBeans>();
        try {
        	con = DBManager.getConnection();
        	String sql = "select name, price, image from buy_detail join item"
        			+ " on buy_detail.item_id = item.id where buy_id = ?";
        	PreparedStatement ps = con.prepareStatement(sql);
        	ps.setString(1, buyId);
        	ResultSet rs = ps.executeQuery();
        	//戻り値がリストの場合whileで取得する
        	while (rs.next()) {
        		ItemBeans item = new ItemBeans();
        		item.setName(rs.getString("name"));
        		item.setPrice(rs.getInt("price"));
        		item.setImage(rs.getString("image"));
        		itemList.add(item);
        	}
        	return itemList;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
	}

}
