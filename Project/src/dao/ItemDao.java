package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.ItemBeans;

public class ItemDao {
	//商品リスト
	public List<ItemBeans> findItemInfo() {
		Connection con = null;
        try {
        	List<ItemBeans> itemList = new ArrayList<ItemBeans>();
        	con = DBManager.getConnection();
        	String sql = "select * from item";

        	Statement stmt = con.createStatement();
        	ResultSet rs = stmt.executeQuery(sql);

        	while (rs.next()) {
        		int id = rs.getInt("id");
        		String name = rs.getString("name");
        		int price = rs.getInt("price");
        		String image = rs.getString("image");
        		ItemBeans itembeans = new ItemBeans(id, name, price, image);
        		itemList.add(itembeans);
        	}

        	return itemList;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
	}

	//商品詳細情報を取得
	public ItemBeans findItemDetail(String id) {

		Connection con = null;
        try {
        	con = DBManager.getConnection();

        	String sql = "select id, name, detail, price, image, stock from item where id = ?";

        	PreparedStatement pstmt = con.prepareStatement(sql);
        	pstmt.setString(1, id);
        	ResultSet rs = pstmt.executeQuery();

        	ItemBeans itemDetail = new ItemBeans();
        	if (rs.next()) {
        	itemDetail.setId(rs.getInt("id"));
    		itemDetail.setName(rs.getString("name"));
    		itemDetail.setDetail(rs.getString("detail"));
    		itemDetail.setPrice(rs.getInt("price"));
    		itemDetail.setImage(rs.getString("image"));
    		itemDetail.setStock(rs.getInt("stock"));

        	}
        	return itemDetail;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
	}

	//商品検索するメソッド
	public ArrayList<ItemBeans> searchItem(String searchKeyword, String price1, String price2) {
		Connection con = null;
		ArrayList<ItemBeans> searchItemList = new ArrayList<ItemBeans>();
        try {
        	con = DBManager.getConnection();
        	String sql = "select * from item where name like '%" + searchKeyword + "%'";

        	if (!price1.equals("")) {
        		sql += " and price >=" + price1;
        	}
        	if (!price2.equals("")) {
        		sql += " and price <=" + price2;
        	}

        	Statement st = con.createStatement();
        	ResultSet rs = st.executeQuery(sql);

        	while (rs.next()) {
        		ItemBeans item = new ItemBeans();
        		item.setId(rs.getInt("id"));
        		item.setName(rs.getString("name"));
        		item.setDetail(rs.getString("detail"));
        		item.setPrice(rs.getInt("price"));
        		item.setImage(rs.getString("image"));
        		searchItemList.add(item);
        	}

        	return searchItemList;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
	}

	/*
	 *
	 *
	 *
	 * 以下、商品マスタ用のメソッド
	 *
	 *
	 *
	 * */

	//商品一覧を取得
	public ArrayList<ItemBeans> getItemList() {
		Connection con = null;
		ArrayList<ItemBeans> itemList = new ArrayList<ItemBeans>();
        try {
        	con = DBManager.getConnection();
        	String sql = "select * from item";
        	Statement st = con.createStatement();
        	ResultSet rs = st.executeQuery(sql);

        	while (rs.next()) {
        		ItemBeans ib = new ItemBeans();
        		ib.setId(rs.getInt("id"));
        		ib.setName(rs.getString("name"));
        		ib.setPrice(rs.getInt("price"));
        		ib.setStock(rs.getInt("stock"));
        		ib.setImage(rs.getString("image"));
        		itemList.add(ib);
        	}
        	return itemList;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
	}

	//アイテム情報の更新
	public int updateItemInfo(String id, String name, String price, String detail, String stock, String image) {
		Connection con = null;
        try {
        	con = DBManager.getConnection();
        	String sql = "update item set name = ?, price = ?, detail = ?, stock = ?, image = ? where id = ?";
        	PreparedStatement ps = con.prepareStatement(sql);
        	ps.setString(1, name);
        	ps.setString(2, price);
        	ps.setString(3, detail);
        	ps.setString(4, stock);
        	ps.setString(5, image);
        	ps.setString(6, id);

        	int result = ps.executeUpdate();
        	if (result <= 0) {
        		return 0;
        	}

        	return result;

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
	}

	//アイテム情報を削除
	public int itemDeleteAction(String id) {
		Connection con = null;
        try {
        	con = DBManager.getConnection();
        	String sql = "delete from item where id = ?";

        	PreparedStatement ps = con.prepareStatement(sql);
        	ps.setString(1, id);
        	int result = ps.executeUpdate();

        	return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
	}

	//アイテム新規登録
	public int itemRegist(String name, String price, String detail, String stock, String image) {
		Connection con = null;
        try {
        	con = DBManager.getConnection();
        	String sql = "insert into item (name, detail, price, image, stock) values (?, ?, ?, ?, ?)";
        	PreparedStatement ps = con.prepareStatement(sql);
        	ps.setString(1, name);
        	ps.setString(2, detail);
        	ps.setString(3, price);
        	ps.setString(4, image);
        	ps.setString(5, stock);
        	int result = ps.executeUpdate();

        	return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
	}

	//商品情報を更新する
	public int updateItemInfo1(String id, String name, String price, String detail, String stock, String image) {
		Connection con = null;
        try {
        	con = DBManager.getConnection();
        	String sql = "update item set";
        	if (!name.equals("")) {
        		sql += " name = '" + name + "'";
        	}
        	if (!name.equals("") && (!price.equals("") || !detail.equals("") || !stock.equals("") || !image.equals(""))) {
        		sql += ",";
        	}
        	if (!price.equals("")) {
        		sql += " price = '" + price + "'";
        	}
        	if (!price.equals("") && (!detail.equals("") || !stock.equals("") || !image.equals(""))) {
        		sql += ",";
        	}
        	if (!detail.equals("")) {
        		sql += " detail = '" + detail + "'";
        	}
        	if (!detail.equals("") && (!stock.equals("") || !image.equals(""))) {
        		sql += ",";
        	}
        	if (!stock.equals("")) {
        		sql += " stock = '" + stock + "'";
        	}
        	if (!stock.equals("") && !image.equals("")) {
        		sql += ",";
        	}
        	if (!image.equals("")) {
        		sql += " image = '" + image + "'";
        	}

        	sql += " where id = " + id;

        	Statement st = con.createStatement();
        	int result = st.executeUpdate(sql);

        	return result;


        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
	}

}
