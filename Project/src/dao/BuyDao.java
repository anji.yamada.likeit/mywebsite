package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.BuyBeans;

public class BuyDao {

	public int insertBuyData(BuyBeans buyData) throws SQLException {
		Connection con = null;
		int autoIncKey = -1;
        try {
        	con = DBManager.getConnection();
        	String sql = "insert into buy (total_price, buy_date, user_id, delivery_method_id) values (?, now(), ?, ?)";
        	PreparedStatement pstmt = null;
        	pstmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        	pstmt.setInt(1, buyData.getTotalPrice());
        	pstmt.setInt(2, buyData.getUserId());
        	pstmt.setInt(3, buyData.getDeliveryMethodId());
        	pstmt.executeUpdate();
        	ResultSet rs = pstmt.getGeneratedKeys();
        	if (rs.next()) {
        		autoIncKey = rs.getInt(1);
        	}

        	return autoIncKey;


        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException(e);
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
	}

	public BuyBeans getBuyData(int buyId) {

		Connection con = null;
		PreparedStatement ps = null;
		BuyBeans buydata = new BuyBeans();
        try {
        	con = DBManager.getConnection();
        	String sql = "select total_price, buy_date, name, price from buy join delivery_method"
        			+ " on buy.delivery_method_id = delivery_method.id where buy.id = ?";

        	ps = con.prepareStatement(sql);
        	ps.setInt(1, buyId);
        	ResultSet rs = ps.executeQuery();

        	if (rs.next()) {
        		buydata.setDeliveryMethodPrice(rs.getInt("price"));
        		buydata.setTotalPrice(rs.getInt("total_price"));
        		buydata.setBuyDate(rs.getTimestamp("buy_date"));
        		buydata.setDeliveryMethodName(rs.getString("name"));
        	}

        	return buydata;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
	}

	public ArrayList<BuyBeans> getBuyHistory() {
		Connection con = null;
		Statement st = null;
		ArrayList<BuyBeans> buyHistoryList = new ArrayList<BuyBeans>();

        try {
        	con = DBManager.getConnection();
        	String sql = "select buy.id, buy_date, name, total_price, price from buy join delivery_method"
        			+ " on buy.delivery_method_id = delivery_method.id order by buy_date desc";
        	st = con.createStatement();
        	ResultSet rs = st.executeQuery(sql);

        	while (rs.next()) {
        		BuyBeans buyHistoryData = new BuyBeans();
        		buyHistoryData.setId(rs.getInt("buy.id"));
        		buyHistoryData.setBuyDate(rs.getTimestamp("buy_date"));
        		buyHistoryData.setDeliveryMethodName(rs.getString("name"));
        		buyHistoryData.setTotalPrice(rs.getInt("total_price"));
        		buyHistoryData.setDeliveryMethodPrice(rs.getInt("price"));
        		buyHistoryList.add(buyHistoryData);
        	}

        	return buyHistoryList;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
	}

	public ArrayList<BuyBeans> getBuyHistoryByUserId(int userId) {
		Connection con = null;
		ArrayList<BuyBeans> buyHistoryList = new ArrayList<BuyBeans>();
        try {
        	con = DBManager.getConnection();
        	String sql = "select buy.id, buy_date, total_price, price from buy"
        			+ " join delivery_method on buy.delivery_method_id = delivery_method.id"
        			+ " where  buy.user_id = ? order by buy_date desc";
        	PreparedStatement ps = con.prepareStatement(sql);
        	ps.setInt(1, userId);
        	ResultSet rs = ps.executeQuery();

        	while (rs.next()) {
        		BuyBeans bb = new BuyBeans();
        		bb.setId(rs.getInt("id"));
        		bb.setBuyDate(rs.getTimestamp("buy_date"));
        		bb.setTotalPrice(rs.getInt("total_price"));
        		bb.setDeliveryMethodPrice(rs.getInt("price"));
        		buyHistoryList.add(bb);
        	}

        	return buyHistoryList;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }

	}

}
