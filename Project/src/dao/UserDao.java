package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import model.UserBeans;

public class UserDao {
	//ログイン情報を検索
	public UserBeans findByLoginInfo(String loginId, String password) {
		Connection con = null;
        try {
        	con = DBManager.getConnection();
        	String sql = "select id, login_id from user where login_id = ? and password = ?";

        	PreparedStatement pStmt = con.prepareStatement(sql);
        	pStmt.setString(1, loginId);
        	pStmt.setString(2, password);
        	ResultSet rs = pStmt.executeQuery();

        	if (!rs.next()) {
        		return null;
        	}

        	int id = rs.getInt("id");
    		String loginid = rs.getString("login_id");
    		UserBeans userBeans = new UserBeans(id, loginid);
    		return userBeans;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
	}

	//新規登録時入力されたログインIDが重複していないかどうかを確認
	public int confirmLoginId(String loginId) {
		Connection con = null;
        try {
        	con = DBManager.getConnection();
        	String sql = "select count(*) from user where login_id = ?";

        	PreparedStatement ps = con.prepareStatement(sql);
        	ps.setString(1, loginId);

        	ResultSet rs = ps.executeQuery();
        	int result = 0;
        	if (rs.next()) {
        		result = rs.getInt("count(*)");
        	}

        	return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }

	}

	//ユーザー新規登録
	public int userSignUp(String loginId, String emailAddress, String password) {
		Connection con = null;
        try {
        	con = DBManager.getConnection();
        	String sql = "insert into user (login_id, email_address, password, create_date) values (?, ?, ?, now())";

        	PreparedStatement pStmt = con.prepareStatement(sql);
        	pStmt.setString(1, loginId);
        	pStmt.setString(2, emailAddress);
        	pStmt.setString(3, password);
        	int result = pStmt.executeUpdate();

        	if (result == 0) {
        		return 0;
        	}

        	return 1;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
	}

	//会員詳細情報を取得
	public UserBeans findUserDetail(int id) {
		Connection con = null;
        try {
        	con = DBManager.getConnection();

        	String sql = "select * from user where id = ?";
        	PreparedStatement pStmt = con.prepareStatement(sql);
        	pStmt.setInt(1, id);
        	ResultSet rs = pStmt.executeQuery();

        	if (!rs.next()) {
        		return null;
        	}

        	int Id = rs.getInt("id");
        	String loginId = rs.getString("login_id");
        	String emailAddress = rs.getString("email_address");
        	String password = rs.getString("password");
        	Date createDate = rs.getDate("create_date");


        	UserBeans userBeans = new UserBeans(Id, loginId, emailAddress, password, createDate);
        	return userBeans;


        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
	}

	//会員情報更新
	public int userInfoUpdate(String emailAddress, String password, int id) {
		Connection con = null;
        try {
        	con = DBManager.getConnection();
        	String sql = null;
        	if (password.equals("") && !emailAddress.equals("")) {
        		sql = "update user set email_address = ? where id = ?";
        		PreparedStatement pstmt = con.prepareStatement(sql);
        		pstmt.setString(1, emailAddress);
        		pstmt.setInt(2, id);
        		int result = pstmt.executeUpdate();
        		return result;
        	} else if (emailAddress.equals("") && !password.equals("")) {
        		sql = "update user set password = ? where id = ?";
        		PreparedStatement pstmt = con.prepareStatement(sql);
        		pstmt.setString(1, password);
        		pstmt.setInt(2, id);
        		int result = pstmt.executeUpdate();
        		return result;
        	} else {
        		sql = "update user set email_address = ? , password = ? where id = ?";
        		PreparedStatement pstmt = con.prepareStatement(sql);
            	pstmt.setString(1, emailAddress);
            	pstmt.setString(2, password);
            	pstmt.setInt(3, id);
            	int result = pstmt.executeUpdate();
            	return result;
        	}

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
	}

	//emailAddressが既に使われているかどうかを確認する
	public int confirmEmailAddress(String emailAddress) {
		Connection con = null;
        try {
        	con = DBManager.getConnection();
        	String sql = "select count(*) from user where email_address = ?";
        	PreparedStatement ps = con.prepareStatement(sql);
        	ps.setString(1, emailAddress);
        	ResultSet rs = ps.executeQuery();
        	int result = 0;
        	if (rs.next()) {
        		result = rs.getInt("count(*)");
        	}

        	return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
	}

}
