package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ItemDao;
import model.UserBeans;

/**
 * Servlet implementation class ItemUpdateMaster
 */
@WebServlet("/ItemUpdateMaster")
public class ItemUpdateMaster extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemUpdateMaster() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログイン情報を取得
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans)session.getAttribute("userInfo");

		//この画面に管理者以外がアクセスすると商品一覧に遷移する
		if (userInfo == null || !userInfo.getLoginId().equals("admin")) {
			response.sendRedirect("Index");
		} else {
			//doPostで使うためのアイテムIDを取得
			String id = request.getParameter("id");

			//リクエストスコープにセットしてフォワード
			request.setAttribute("id", id);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemUpdateMaster.jsp");
			dispatcher.forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String price = request.getParameter("price");
		String detail = request.getParameter("detail");
		String stock = request.getParameter("stock");
		String image = request.getParameter("image");

		if (!name.equals("") && !price.equals("") && !detail.equals("")
				&& !stock.equals("") && !image.equals("")) {
			request.setAttribute("errorMessage", "入力内容が正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemUpdateMaster.jsp");
			dispatcher.forward(request, response);
		}

		ItemDao itemDao = new ItemDao();
		int result = itemDao.updateItemInfo1(id, name, price, detail, stock, image);

		if (result == 0) {
			request.setAttribute("errorMessage", "入力内容が正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemUpdateMaster.jsp");
			dispatcher.forward(request, response);
		} else {
			response.sendRedirect("ItemMaster");
		}

	}

}
