package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

/**
 * Servlet implementation class SignUp
 */
@WebServlet("/SignUp")
public class SignUp extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUp() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignUp.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//リクエストパラメーターを取得する
		String loginId = request.getParameter("loginId");
		String emailAddress = request.getParameter("emailAddress");
		String password = request.getParameter("password");
		String passwordConfirmation = request.getParameter("passwordConfirmation");

		//loginIdがDBにあるものと重複していないか確認する
		UserDao userDao = new UserDao();
		int confirmLoginId = userDao.confirmLoginId(loginId);

		//パスワードとパスワード確認が不一致またはどれか一つでも未入力の場合は新規登録画面に遷移する
		if (!password.equals(passwordConfirmation) || loginId.equals("") || emailAddress.equals("") ||
				password.equals("") || passwordConfirmation.equals("")) {
			request.setAttribute("errorMessage", "入力内容が正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignUp.jsp");
			dispatcher.forward(request, response);
		} else if (confirmLoginId != 0) {
			request.setAttribute("errorMessage", "入力されたログインIDは既に存在しています");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignUp.jsp");
			dispatcher.forward(request, response);
		} else {
			//入力されたパスワードをMD5化する
			String encryptPassword = MyWebSiteHelper.encryptPassword(password);
			//リクエストパラメーターを引数にしてdaoメソッドを呼ぶ
			int result = userDao.userSignUp(loginId, emailAddress, encryptPassword);
			//データを登録できなかった場合の処理
			if (result == 0) {
				request.setAttribute("errorMessage", "入力内容が正しくありません");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignUp.jsp");
				dispatcher.forward(request, response);
			}
			//会員登録に成功したらセッションに情報をセットする
			response.sendRedirect("Index");
		}

	}

}
