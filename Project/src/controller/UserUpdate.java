package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.UserBeans;

/**
 * Servlet implementation class UserUpdate
 */
@WebServlet("/UserUpdate")
public class UserUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans)session.getAttribute("userInfo");
		UserDao userDao = new UserDao();
		UserBeans createDate = userDao.findUserDetail(userInfo.getId());

		request.setAttribute("createDate", createDate);
		//jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans)session.getAttribute("userInfo");

		String emailAddress = request.getParameter("emailAddress");
		String password = request.getParameter("password");
		String passwordConfirmation = request.getParameter("passwordConfirmation");

		//Eメールアドレスが重複していないかを確認する処理
		UserDao userDao = new UserDao();
		int confirmDuplicatedEmail = userDao.confirmEmailAddress(emailAddress);

		if (!password.equals(passwordConfirmation) || (password.equals("")
				&& passwordConfirmation.equals("") && emailAddress.equals(""))) {
			request.setAttribute("errorMessage", "入力内容が正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
		} else if (confirmDuplicatedEmail > 0) {
			//Eメールアドレスが既に使われていたらエラーメッセージを表示する
			request.setAttribute("errorMessage", "入力されたEメールアドレスは既に使われています");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
		} else {
			//入力されたパスワードをMD5化する
			String encryptPassword = MyWebSiteHelper.encryptPassword(password);
			int result = userDao.userInfoUpdate(emailAddress, encryptPassword, userInfo.getId());

			response.sendRedirect("UserDetail");
		}

	}
}
