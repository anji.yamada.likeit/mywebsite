package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import dao.ItemDao;
import model.UserBeans;

/**
 * Servlet implementation class ItemRegistMaster
 */
@WebServlet("/ItemRegistMaster")
//imageの保存先
@MultipartConfig(location="/Users/yamadaanji/Documents/MyWebSite/Project/WebContent/WEB-INF/image", maxFileSize=1048576)
public class ItemRegistMaster extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemRegistMaster() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログイン情報を取得
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans)session.getAttribute("userInfo");

		/*管理者以外がアクセスして勝手に商品情報を登録されてしまっては困る */
		//管理者以外は商品一覧に遷移する
		if (userInfo == null || !userInfo.getLoginId().equals("admin")) {
			response.sendRedirect("Index");
		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemRegistMaster.jsp");
			dispatcher.forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String name = request.getParameter("name");
		String price = request.getParameter("price");
		String detail = request.getParameter("detail");
		String stock = request.getParameter("stock");

		//フォームからimageを取得する
		Part part = request.getPart("image");
		String PartName = this.getFileName(part);
		//保存するもの
        part.write(PartName);

		//一つでも未入力の場合はエラーメッセージを表示する
        //partはnullにならないらしい
		if (name.equals("") || price.equals("") || detail.equals("") || stock.equals("") || part.getSize() == 0) {
			request.setAttribute("registActionMessage", "全ての項目を入力してください");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemRegistMaster.jsp");
			dispatcher.forward(request, response);
		} else {
			ItemDao itemDao = new ItemDao();
			itemDao.itemRegist(name, price, detail, stock, PartName);
			request.setAttribute("registActionMessage", "登録に成功しました");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemRegistMaster.jsp");
			dispatcher.forward(request, response);
		}

	}

	//リクエストからjavax.servlet.http.Partを取得
	private String getFileName(Part part) {
        String name = null;
        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
            if (dispotion.trim().startsWith("filename")) {
                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
                name = name.substring(name.lastIndexOf("\\") + 1);
                break;
            }
        }
        return name;
    }

}
