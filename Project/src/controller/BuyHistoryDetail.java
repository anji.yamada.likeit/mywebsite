package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.BuyDetailDao;
import model.BuyBeans;
import model.ItemBeans;

/**
 * Servlet implementation class BuyHistoryDetail
 */
@WebServlet("/BuyHistoryDetail")
public class BuyHistoryDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyHistoryDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//購入IDを取得する
		String buyId = request.getParameter("buyId");
		//購入IDを元に必要なデータを取得する
		BuyDetailDao bdd = new BuyDetailDao();
		BuyBeans buyData = bdd.getBuyDataByBuyId(buyId);

		ArrayList<ItemBeans> itemList = bdd.getBuyDetailByBuyId(buyId);

		request.setAttribute("buyData", buyData);
		request.setAttribute("itemList", itemList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyHistoryDetail.jsp");
		dispatcher.forward(request, response);

	}

}
