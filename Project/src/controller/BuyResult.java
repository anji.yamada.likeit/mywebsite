package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BuyDao;
import dao.BuyDetailDao;
import model.BuyBeans;
import model.BuyDetailBeans;
import model.ItemBeans;

/**
 * Servlet implementation class BuyResult
 */
@WebServlet("/BuyResult")
public class BuyResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//BuyResult doGet購入情報を登録する処理

		HttpSession session = request.getSession();
		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>)session.getAttribute("cart");

		int userId = Integer.parseInt(request.getParameter("id"));
		int deliveryMethodId = Integer.parseInt(request.getParameter("deliveryMethodId"));
		//購入情報登録に必要な合計金額
		int totalPrice = 0;
		for (ItemBeans item: cart) {
			totalPrice += item.getPrice();
		}
		//購入情報をbeansにセットする
		BuyBeans buyData = new BuyBeans();
		buyData.setUserId(userId);
		buyData.setTotalPrice(totalPrice);
		buyData.setDeliveryMethodId(deliveryMethodId);

		//購入情報を登録
		BuyDao buyDao = new BuyDao();
		int buyId;
		try {
			buyId = buyDao.insertBuyData(buyData);

			//購入IDに紐づく詳細を登録
			for (ItemBeans item: cart) {
				BuyDetailBeans bdb = new BuyDetailBeans();
				bdb.setBuyId(buyId);
				bdb.setItemId(item.getId());
				BuyDetailDao bdd = new BuyDetailDao();
				bdd.insertBuyDetail(bdb);
			}

			//購入完了画面で表示するためのデータを取得する処理
			BuyDao bd = new BuyDao();
			BuyBeans buyInfo = bd.getBuyData(buyId);

			request.setAttribute("buyInfo", buyInfo);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyResult.jsp");
			dispatcher.forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
