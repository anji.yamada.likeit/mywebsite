package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DeliveryMethodDao;
import model.DeliveryMethodBeans;
import model.ItemBeans;
import model.UserBeans;

/**
 * Servlet implementation class BuyConfirm
 */
@WebServlet("/BuyConfirm")
public class BuyConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans)session.getAttribute("userInfo");

		//ログイン情報がない場合はログイン画面、ある場合は商品一覧画面に遷移
		if (userInfo == null) {
			response.sendRedirect("Login");
		} else {
			response.sendRedirect("Index");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		//ログイン情報を取得
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans)session.getAttribute("userInfo");

		//カートを取得
		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>)session.getAttribute("cart");

		//ログインしていないまたはカートに商品がない状態では購入できない処理
		if (userInfo == null || cart.size() == 0) {
			request.setAttribute("cartActionMessage", "ログイン情報またはカートに商品がありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
			dispatcher.forward(request, response);
		}

		//配送方法を取得
		DeliveryMethodDao deliveryMethodDao = new DeliveryMethodDao();
		DeliveryMethodBeans deliveryMethod = deliveryMethodDao.findDeliveryMethod();


		//合計金額を格納するための変数
		int totalPrice = 0;
		for (ItemBeans itemPrice: cart) {
			totalPrice += itemPrice.getPrice();
		}
		//合計金額をフォーマットする
		String formatTotalPrice = String.format("%,d", (int)((totalPrice + 500) * 1.1));

		request.setAttribute("formatTotalPrice", formatTotalPrice);
		request.setAttribute("deliveryMethod", deliveryMethod);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyConfirm.jsp");
		dispatcher.forward(request, response);

	}

}
