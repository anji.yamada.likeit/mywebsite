package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BuyDao;
import dao.UserDao;
import model.BuyBeans;
import model.UserBeans;

/**
 * Servlet implementation class UserDetail
 */
@WebServlet("/UserDetail")
public class UserDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログイン情報があるかどうかを確認
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans)session.getAttribute("userInfo");

		if (userInfo != null) {
			//ユーザー情報を取得
			UserDao userDao = new UserDao();
			UserBeans userBeans = userDao.findUserDetail(userInfo.getId());

			//購入履歴情報を取得
			BuyDao buyHistoryData = new BuyDao();
			ArrayList<BuyBeans> buyHistoryList = buyHistoryData.getBuyHistoryByUserId(userInfo.getId());
			
			//リクエストスコープにセット
			request.setAttribute("userDetail", userBeans);
			request.setAttribute("buyHistoryList", buyHistoryList);

			//jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetail.jsp");
			dispatcher.forward(request, response);
		} else {
			request.setAttribute("errorMessage", "ログイン情報がありません。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetail.jsp");
			dispatcher.forward(request, response);
		}

	}

}
