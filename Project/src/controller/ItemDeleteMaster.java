package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ItemDao;
import model.ItemBeans;

/**
 * Servlet implementation class ItemDeleteMaster
 */
@WebServlet("/ItemDeleteMaster")
public class ItemDeleteMaster extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemDeleteMaster() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");

		ItemDao itemDao = new ItemDao();
		ItemBeans itemInfo = itemDao.findItemDetail(id);

		request.setAttribute("deleteActionMessage", "本当にこの商品を削除しますか?");
		request.setAttribute("itemInfo", itemInfo);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemDeleteMaster.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		ItemDao itemDao = new ItemDao();

		String deleteAction = request.getParameter("deleteAction");
		if (deleteAction != null) {
			itemDao.itemDeleteAction(id);
			request.setAttribute("deleteActionMessage", "アイテムを削除しました");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemDeleteMaster.jsp");
			dispatcher.forward(request, response);
		} else {
			response.sendRedirect("ItemMaster");
		}

	}

}
