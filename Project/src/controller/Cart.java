package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ItemDao;
import model.ItemBeans;

/**
 * Servlet implementation class Cart
 */
@WebServlet("/Cart")
public class Cart extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Cart() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//カートが空になったらメッセージを表示する
		HttpSession session = request.getSession();
		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>)session.getAttribute("cart");
		//リストの要素がゼロならカート内は空
		if (cart == null || cart.size() == 0) {
			request.setAttribute("cartActionMessage", "カートに商品がありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
			dispatcher.forward(request, response);
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		//商品詳細画面からカートに入れると商品idを取得
		String id = request.getParameter("Id");

		//カートに追加した商品情報を取得
		ItemDao itemDao = new ItemDao();
		ItemBeans item = itemDao.findItemDetail(id);

		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>)session.getAttribute("cart");
		//カートが存在しない場合は作成する
		if (cart == null) {
			cart = new ArrayList<ItemBeans>();
		}

		//同じ商品が追加されたらメッセージを表示
		for (ItemBeans itemInfo: cart) {
			if (itemInfo.getId() == item.getId()) {
				request.setAttribute("sameItem", "同じ商品がカートにあります");
			}
		}

		//カートに商品を追加する
		cart.add(item);
		session.setAttribute("cart", cart);

		request.setAttribute("cartActionMessage", "カートに商品を追加しました");
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
		dispatcher.forward(request, response);

	}
}
