package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ItemDao;
import model.ItemBeans;

/**
 * Servlet implementation class SearchResult
 */
@WebServlet("/SearchResult")
public class SearchResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//この画面に直接アクセスした場合商品一覧に遷移する
		response.sendRedirect("Index");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//検索ワードを取得
		String searchKeyword = request.getParameter("searchKeyword");
		String price1 = request.getParameter("price1");
		String price2 = request.getParameter("price2");

		//検索結果を取得
		ItemDao itemDao = new ItemDao();
		ArrayList<ItemBeans> searchItemList = itemDao.searchItem(searchKeyword, price1, price2);

		//アイテム総数を取得してスコープにセット
		request.setAttribute("itemTotalNum", searchItemList.size());
		request.setAttribute("searchItemList", searchItemList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/searchResult.jsp");
		dispatcher.forward(request, response);

	}

}
