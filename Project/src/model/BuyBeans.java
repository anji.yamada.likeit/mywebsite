package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BuyBeans implements Serializable {
	private int id;
	private int totalPrice;
	private Date buyDate;
	private int userId;
	private int deliveryMethodId;

	private String deliveryMethodName;
	private int deliveryMethodPrice;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Date getBuyDate() {
		return buyDate;
	}
	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getDeliveryMethodId() {
		return deliveryMethodId;
	}
	public void setDeliveryMethodId(int deliveryMethodId) {
		this.deliveryMethodId = deliveryMethodId;
	}
	public String getDeliveryMethodName() {
		return deliveryMethodName;
	}
	public void setDeliveryMethodName(String deliveryMethodName) {
		this.deliveryMethodName = deliveryMethodName;
	}
	public int getDeliveryMethodPrice() {
		return deliveryMethodPrice;
	}
	public void setDeliveryMethodPrice(int deliveryMethodPrice) {
		this.deliveryMethodPrice = deliveryMethodPrice;
	}

	//購入日時をフォーマットで表示するための記述
	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
		return sdf.format(buyDate);
	}

	public String getFormatTotalPrice() {
		//税金も追加する
		//int型に変換することでformatメソッドの型に合うようにする
		return String.format("%,d", (int)((this.totalPrice + this.deliveryMethodPrice) * 1.1));
	}

}