package model;

import java.io.Serializable;

public class ItemBeans implements Serializable {
	private int id;
	private String name;
	private String detail;
	private int price;
	private String image;
	private int stock;

	public ItemBeans() {
		super();
	}


	public ItemBeans(int id, String name, int price, String image) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.image = image;
	}

	public ItemBeans(String name, String detail, int price, String image) {
		super();
		this.name = name;
		this.detail = detail;
		this.price = price;
		this.image = image;
	}

	public ItemBeans(String image) {
		super();
		this.image = image;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}

	public String getFormatPrice() {
		return String.format("%,d", this.price);
	}


}
