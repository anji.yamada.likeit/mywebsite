package model;

import java.io.Serializable;
import java.util.Date;

public class UserBeans implements Serializable {
	private int id;
	private String loginId;
	private String emailAddress;
	private String password;
	private Date createDate;

	public UserBeans(int id, String loginId) {
		super();
		this.id = id;
		this.loginId = loginId;
	}

	public UserBeans(int id, String loginId, String emailAddress, String password, Date createDate) {
		super();
		this.id = id;
		this.loginId = loginId;
		this.emailAddress = emailAddress;
		this.password = password;
		this.createDate = createDate;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


}
